﻿DROP DATABASE IF EXISTS ejemplo10;
CREATE DATABASE IF NOT EXISTS ejemplo10;
USE ejemplo10;

CREATE TABLE noticias(
  codigo int AUTO_INCREMENT,
  titulo varchar(100),
  texto varchar(1000),
  fecha date,
  PRIMARY KEY(codigo)
);


CREATE TABLE fotos(
  codigo int AUTO_INCREMENT,
  nombre varchar(100),
  PRIMARY KEY(codigo)
);

CREATE TABLE comentarios(
  codigo int AUTO_INCREMENT,
  texto varchar(1000),
  fecha date,
  cod_noticia int NOT NULL,
  PRIMARY KEY(codigo),
  CONSTRAINT fk_comentarios_noticias FOREIGN KEY(cod_noticia) REFERENCES noticias(codigo)
    ON DELETE CASCADE on UPDATE CASCADE
);

CREATE TABLE noticias_fotos (
  cod_noticia int,
  cod_foto int,
  visitas int DEFAULT 0,
  PRIMARY KEY(cod_noticia,cod_foto),
  CONSTRAINT fk_noticiasfotos_noticias FOREIGN KEY (cod_noticia) REFERENCES noticias(codigo)
    ON DELETE CASCADE on UPDATE CASCADE,
  CONSTRAINT fk_noticiasfotos_fotos FOREIGN KEY (cod_foto) REFERENCES fotos(codigo)
    ON DELETE CASCADE on UPDATE CASCADE
);

INSERT INTO noticias (titulo, texto, fecha)
  VALUES ('Noticia 1', 'Ejemplo de noticia', '2021/11/09'),('Noticia 2','segunda noticia','2021/11/10');



