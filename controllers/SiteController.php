<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Noticias;
use app\models\Comentarios;
use app\models\Fotos;
use yii\web\UploadedFile;
use app\models\NoticiasFotos;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        
        $consulta=Noticias::find();
        
        
        $dataProvider=new ActiveDataProvider([
            'query' => $consulta,
        ]);
        
        return $this->render('index',[
            'dataProvider' => $dataProvider
        ]);
    }
    
    public function actionNuevocomentario($codigo){
       $model = new Comentarios();

        if ($model->load(Yii::$app->request->post())) {
            $model->cod_noticia=$codigo;
         if ($model->save()) {
                return $this->redirect(["site/vercomentarios","codigo"=>$codigo]);
            }
        }

        return $this->render('formulario', [
            'model' => $model,
        ]);
        
    }
    
    public function actionNuevanoticia(){
       $model = new Noticias(); // modelo para almacenar los datos 
       
        // pregunto si vengo del formulario 
        if ($model->load(Yii::$app->request->post())) {
         if ($model->save()) { // intentando grabar los datos en tabla
                return $this->redirect(["site/index"]); // cargo la pagina principal
            }
        }

        // cargo el formulario porque
        // es la primera vez que llamo a la accion
        // o porque hay errores
        return $this->render('formularionoticia', [
            'model' => $model,
        ]);
        
    }
    
    public function actionVercomentarios($codigo){
        
        $consulta= Comentarios::find()->where(["cod_noticia"=>$codigo]);
        $noticia= Noticias::findOne($codigo);
        $fotos=$noticia->getCodFotos()->all();
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => $consulta,
            
        ]);
        
        return $this->render("listar",[
            "dataProvider" => $dataProvider,
            "noticia" =>$noticia,
            "fotos" => $fotos,
             
        ]);
    }
    
    public function actionEditarnoticia($codigo) {
        
        /**
         * Cargar los datos de la noticia desde la base de datos
         */
        $noticia= Noticias::findOne($codigo);
                
        /**
         * quiero saber si he pulsado al boton editar desde el listado de noticias
         * o al boton submit del formulario de actualizacion de la noticia
         */
        if($this->request->isPost){
            // aqui llego si he pulsado el boton de actualizar en el formulario de la noticia
            if ($noticia->load(Yii::$app->request->post())) { // cargo los datos del formulario en el modelo
             if ($noticia->save()) { // actualizo la noticia en la base de datos
                    return $this->redirect(["site/index"]);
                }
            }
        }
        
        // aqui solo llega si he pulsado el boton de editar noticia en el listado de noticias
        return $this->render('formularionoticia', [
            'model' => $noticia,
        ]);
    }
    
    
    public function actionEditarcomentario($codigo) {
        
        /**
         * Cargar los datos del comentario desde la base de datos
         */
        $comentario= Comentarios::findOne($codigo);
                
        /**
         * quiero saber si he pulsado al boton editar desde el listado de comentarios
         * o al boton modificar del formulario de actualizacion del comentario
         */
        if($this->request->isPost){
            // aqui llego si he pulsado el boton de actualizar en el formulario del comentario
            if ($comentario->load(Yii::$app->request->post())) { // cargo los datos del formulario en el modelo
             if ($comentario->save()) {
                    return $this->redirect(["site/vercomentarios","codigo"=>$comentario->cod_noticia]);
                }
            }
        }
        
        // aqui solo llega si he pulsado el boton de editar comentario en el listado de comentarios
        return $this->render('formulario', [
            'model' => $comentario,
        ]);
    }
    
    public function actionEliminarcomentario($codigo){
        // cargo el comentario a eliminar de la base de datos
        $comentario= Comentarios::findOne($codigo);
        
        // obtengo la noticia del comentario a eliminar
        $cod_noticia=$comentario->cod_noticia;
        
        // elimino el comentario
        $comentario->delete();
        
        // llamo a la accion que me muestra todos los comentarios de la noticia donde he eliminado el comentario
        return $this->redirect(["site/vercomentarios","codigo"=>$cod_noticia]);
        
    }
    
    public function actionEliminarnoticia($codigo){
        // cargo la noticia a eliminar de la base de datos
        $noticia= Noticias::findOne($codigo);
              
        // elimino la noticia
        $noticia->delete();
        
        // llamo a la accion que me muestra todos los comentarios de la noticia donde he eliminado el comentario
        return $this->redirect(["site/index"]);
        
    }
    
    public function actionConfirmareliminarnoticia($codigo){
        // cargo la noticia a eliminar de la base de datos
        $noticia= Noticias::findOne($codigo);
              
                
        // llamo a la accion que me muestra todos los comentarios de la noticia donde he eliminado el comentario
        return $this->render("ver",[
            "model" => $noticia
        ]);
        
    }
    
    /*
     * Metodo para añadir imagenes a la base de datos
     * Añade la imagen a la carpeta web/imgs
     * Y en la base de datos colocamos el nombre de la imagen
     */
    
    public function actionFotos(){
        $model=new Fotos();
        
        // comprobando si vengo del formulario
        if ($model->load(Yii::$app->request->post())) {
            $model->nombre= UploadedFile::getInstance($model, 'nombre');
         if ($model->save()) { // intentando grabar los datos en tabla
                return $this->redirect(["site/fotos"]); // cargo la pagina principal
            }
        }
        
        
        // cargo el formulario la primera vez o cuando hay errores
        return $this->render("fotos",[
            "model" => $model
        ]);
    }
    
    public function actionListarfotos(){
        
        $activeDataProvider=new ActiveDataProvider([
            "query" => Fotos::find(),
        ]);
        
        return $this->render("listarfotos",[
            "dataProvider" => $activeDataProvider
        ]);
    }
    
    public function actionNoticiasfotos()
    {
        // este modelo me permite almacenar todos los datos del formulario
        $model = new NoticiasFotos();
        
        // compruebo si vengo del formulario
        if ($model->load(Yii::$app->request->post())) {
            $correcto=[]; // array con las imagenes que pueda añadir a la noticia
            
            // en cod_foto tengo un array con todas las fotos seleccionadas
            foreach ($model->cod_foto as $foto){
                
                // creo un modelo para cada uno de los registros de noticiasfotos
                $modelo=new NoticiasFotos();
                $modelo->cod_noticia=$model->cod_noticia;
                $modelo->cod_foto=$foto;
                
                if($modelo->save()){
                    // si puedo almacenar el registro
                    // meto en el array correcto el nombre de la foto asociada a la noticia
                    $correcto[]=Fotos::findOne($foto)->nombre;
                }
            }
            
            // en caso de que no pueda almacenar niguno de los registros
            // porque todas las fotos ya estan en la noticia
            // coloco un error
            if (count($correcto)>0) {
                // vista donde me muestra las fotos añadidas
                return $this->render('salidanoticiafotos',[
                    'correctas'=>$correcto,
                ]);
            }else{
                // añadir un error al modelo en el campo cod_foto
                $model->addError("cod_foto","todas las fotos estan ya en la noticia");
            }
        }
        return $this->render('noticiasfotos', [
            'model' => $model,
        ]);
    }
    
    public function actionEditarfoto($codigo){
        $model=Fotos::findOne($codigo);
        
        if($this->request->post()){
            // comprobando si vengo del formulario
            if ($model->load(Yii::$app->request->post())) {
                $model->nombre= UploadedFile::getInstance($model, 'nombre');
            if ($model->save()) { // intentando grabar los datos en tabla
                    return $this->redirect(["site/fotos"]); // cargo la pagina principal
                }
            }
        }
        
        // cargo el formulario la primera vez o cuando hay errores
        return $this->render("fotos",[
            "model" => $model
        ]);
        
    }
    
    public function actionEliminarfoto($codigo){
        $model= Fotos::findOne($codigo);
        $model->delete();
        return $this->redirect(['site/listarfotos']);
    }
   
}
