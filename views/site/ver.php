 <?php
 
 use yii\widgets\DetailView;
 use yii\helpers\Html;
 
 echo DetailView::widget([
     "model" => $model
 ]);
 
 ?>
<div>
<?php
 // colocar el boton para eliminar la noticia
echo Html::a("¿Estas seguro de eliminar la noticia?",
        ["site/eliminarnoticia","codigo"=>$model->codigo],
        ["class"=>"btn btn-danger col-lg-4"]);
?>   
</div>
