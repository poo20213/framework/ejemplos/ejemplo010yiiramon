<?php

use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    "dataProvider" => $dataProvider,
    "layout"=>"{items}",
    "options" => ['class' => 'text-center'],
    "columns" =>[
        'codigo',
        [
            'label'=>'Foto',
            'format'=>'raw',
            
            'value' => function($model){
                return Html::img('@web/imgs/' . $model->nombre ,['class' => 'mx-auto col-lg-6']); 
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' => [
                'update' => function ($url,$model) {
                    return Html::a('<i class="far fa-edit"></i>', 
                       ['site/editarfoto',"codigo"=>$model->codigo]
                       );
                },
                'delete' => function ($url,$model) {
                    return Html::a('<i class="fas fa-trash"></i>',
                            ['site/eliminarfoto',"codigo"=>$model->codigo],
                            [
                              'data' => [
                                  'confirm' => '¿Seguro que deseas eliminar la foto?',
                                  'method' => 'post',
                              ]  
                            ]);
                },
	        ],
        ]
    ]
]);